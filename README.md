# LPS-laser

Container with IOC for TU-LPS laser from EKSPLA

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-containers/lps-laser/lpslaser:latest
    container_name: laser
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/lps-laser/iocBoot/ioclaser"
    command: "./laser.cmd"
    volumes:
      - type: bind
        source: /EPICS/autosave/laser
        target: /EPICS/autosave
    environment:
      - DEVIPA=0.0.0.0
      - DEVIPB=0.0.0.0
      - DEVPORT=8081
      - IOCBL=LPQ
      - IOCDEV=laser
```
