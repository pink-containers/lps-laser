#!../../bin/linux-x86_64/laser

## You may have to change laser to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/laser.dbd"
laser_registerRecordDeviceDriver pdbbase

epicsEnvSet ("STREAM_PROTOCOL_PATH", ".")
epicsEnvSet ("ASYNPORT", "LL")
#epicsEnvSet ("DEVIPA", "127.0.0.1")
#epicsEnvSet ("DEVIPB", "127.0.0.1")
epicsEnvSet ("DEVIPA", "192.168.0.118")
epicsEnvSet ("DEVIPB", "192.168.0.128")
epicsEnvSet ("DEVPORT", "8081")
epicsEnvSet ("IOCBL", "LPQ")
epicsEnvSet ("IOCDEV", "laser")

## Asyn ports
drvAsynIPPortConfigure("${ASYNPORT}A", "${DEVIPA}:${DEVPORT} HTTP",0,0,0)
drvAsynIPPortConfigure("${ASYNPORT}B", "${DEVIPB}:${DEVPORT} HTTP",0,0,0)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asynA,PORT=${ASYNPORT}A,ADDR=0,IMAX=512,OMAX=512")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asynB,PORT=${ASYNPORT}B,ADDR=0,IMAX=512,OMAX=512")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("pump_extra.db","BL=${IOCBL},DEV=${IOCDEV}:pump,ASYNPORT=${ASYNPORT}A,HOST=${DEVIPA}")
dbLoadRecords("pump.db","BL=${IOCBL},DEV=${IOCDEV}:pump,ASYNPORT=${ASYNPORT}A,HOST=${DEVIPA}")
dbLoadRecords("probe.db","BL=${IOCBL},DEV=${IOCDEV}:probe,ASYNPORT=${ASYNPORT}B,HOST=${DEVIPB}")

## autosave
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=${IOCBL},DEV=${IOCDEV}")

## Start any sequence programs
#seq sncxxx,"user=epics"
